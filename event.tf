resource "aws_sns_topic" "main_demo" {
  #checkov:skip=CKV_AWS_26:Ensure all data stored in the SNS topic is encrypted
  
  name              = "demo"
  kms_master_key_id = var.KMS_KEY_ID

 
}


data "aws_iam_policy_document" "sns_topic_policy_demo" {


  statement {
    actions = [
      "SNS:GetTopicAttributes",
      "SNS:SetTopicAttributes",
      "SNS:AddPermission",
      "SNS:RemovePermission",
      "SNS:DeleteTopic",
      "SNS:Subscribe",
      "SNS:ListSubscriptionsByTopic",
      "SNS:Publish",
      "SNS:Receive"
    ]

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    

    resources = [aws_sns_topic.main_demo.arn]
  }
}

resource "aws_sns_topic_policy" "demo" {
  

  arn    = aws_sns_topic.main_demo.arn
  policy = data.aws_iam_policy_document.sns_topic_policy_demo.json
}

resource "aws_sns_topic_subscription" "email-target_demo" {
 
  
  topic_arn = aws_sns_topic.main_demo.arn
  confirmation_timeout_in_minutes = 10
  protocol  = "email"
  endpoint  = "prodipto03ei38@gmail.com"


  filter_policy                   = jsonencode(
            {
  "State": [
    {
      "anything-but": "COMPLETED"
    }
  ]
}
        ) 
}


###################################


resource "aws_cloudwatch_event_rule" "console" {
  
  name        = "demo"
  description = "demo"

  event_pattern = jsonencode({
  "source": ["aws.backup"],
  "detail-type": ["Backup Job State Change"]
})
}

resource "aws_cloudwatch_event_target" "sns" {

  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "SendToSNS"
  arn       = aws_sns_topic.main_demo.arn
}




# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_vault_notifications
resource "aws_backup_vault_notifications" "enable_sns_integration" {
  
  backup_vault_name   = aws_backup_vault.example-backup-vault.id
  sns_topic_arn       = aws_sns_topic.main_demo.arn
  backup_vault_events = var.EVENTS
}


