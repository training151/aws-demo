variable "project" {}




variable "EVENTS" {
  description = "Provide list of backup vault events"
  type        = list(string)
  default = [
    "BACKUP_JOB_FAILED",
    "BACKUP_JOB_EXPIRED",
    "RESTORE_JOB_FAILED",
    "COPY_JOB_FAILED"
  ]
}



variable "KMS_KEY_ID" {
  description = "Provide kms key id used to encrypt sns topic"
  type        = string
  default     = null
}

